# WP Geo Query

Modify the WP_Query to filter the geo_query parameter.


### Prerequisites

* WordPress <= 5.8
* Acf >= 5.9.9


## Getting Started

```
cd mysite/wp-content/plugins/
git clone https://gitlab.com/david-bzh/wp-geo-query.git
```

### Activate plugin

* In https://mysite.com/wp-admin/plugins.php > Activate WP Geo Query



## Installing for dev

```
cd mysite/wp-content/plugins/
composer install
```

## Authors

* **David** -

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
