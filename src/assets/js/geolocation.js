const geolocation = {
	input: {
		geo: document.querySelector(".btn-geolocation"),
		address: document.querySelector(".btn-address"),
		lat: document.querySelector(".btn-geolocation-lat"),
		lng: document.querySelector(".btn-geolocation-lng"),
		autoCompleteJS: {
			maxlength: 4,
		}
	},
	setting: SETTING_SERVICES,
	defaultServices: {
		search: [],
		reverse: [],
	},
	data: {
		latitude: null,
		longitude: null,
		address: null,
	},
	debug: {
		enable: false,
		log: true,
		cache: false,
		defaultServices: {
			search: {
				url: 'http://dev.virtualearth.net/REST/v1/Locations?q=1 Rue Louis Pasteur, 29400 Landivisiau&o=json&key=AvPdyBgvGkEXCZKYpRQNtHXUnr7uzfmA5VceD6-im3KhMB0urFQ3K_JvVEqnnnhj',
			},
			reverse: {
				url: 'https://api-adresse.data.gouv.fr/reverse/?lat=47.1946003594091&long=-1.6243084908209913'
			},
		},
		initService: {
			type: 'search',
			nameService: 'bing',
			data: {},
		},
		data: {
			latitude: 47.1946003594091,
			longitude: 1.6243084908209913,
			address: null,
			search: null,
		},
	}
};


/**
 * Init Input range with noUiSlider
 */
 const nouislider = document.getElementById('nouislider');

if (document.getElementById("wp-geo-form-nouislider-js") && nouislider) {

	const inputRange = document.getElementById('customRange1');

	noUiSlider.create( nouislider, SETTING_NOUISLIDER);

	nouislider.noUiSlider.on('update', function (values, handle) {
		inputRange.value = values;
	});
}

/**
 * Load Data LocalStorage
 */
if (geolocation.input.address) {
	setDataLocalStorage('fetchGeocoding');
}

/**
 * Active Services Providers
 */
Object.keys(geolocation.setting).forEach(providerName => {
	let provider = geolocation.setting[providerName];
	if (provider.enable === 'true') {
		if (provider.search === 'true') {
			geolocation.defaultServices.search.push(providerName);
		}
		if (provider.reverse === 'true') {
			geolocation.defaultServices.reverse.push(providerName);
		}
	}
});

/**
 * Active navigator geolocation
 */
if (geolocation.input.geo) {
	geolocation.input.geo.addEventListener("click", function (e) {
		if (window.location.protocol === 'https:') {
			navigator.geolocation.getCurrentPosition((position) => {
				initService(geolocation.defaultServices.reverse[0], 'reverse');
				if (fetchGeocoding.fetchGeocoding !== undefined) {
					toggleDisabled([geolocation.input.geo, geolocation.input.address]);
					fetchGeocoding();
				}
			});
		} else {
			console.error('This feature is available only in secure contexts (HTTPS), in some or all supporting browsers.');
		}
	});
}

/**
 *
 * @param {string} nameService
 */
function initService(nameService, type = '') {
	switch (setObjectfetchGeocoding(nameService, type)) {
		case 'locationiq': //Region 2: Europe
			geolocation.fetchGeocoding.initService = {
				nameService: 'locationiq',
				data: {},
			};
			if (type === 'reverse') {
				geolocation.fetchGeocoding.initService.url = `https://eu1.locationiq.com/v1/reverse.php?key=${geolocation.setting.locationiq.key}&lat=${geolocation.data.latitude}&lon=${geolocation.data.longitude}&accept-language=${geolocation.setting.locationiq.countrycodes.toLowerCase()}&format=${geolocation.setting.locationiq.format}`;
			} else if (type === 'search') {
				geolocation.fetchGeocoding.initService.url = `https://eu1.locationiq.com/v1/search.php?key=${geolocation.setting.locationiq.key}&q=${geolocation.input.address.value}&countrycodes=${geolocation.setting.locationiq.countrycodes}&format=${geolocation.setting.locationiq.format}&limit=7`;
			}
			break;
		case 'rapidapi':
			geolocation.fetchGeocoding.initService = {
				nameService: 'rapidapi',
				data: {
					"method": geolocation.setting.rapidapi.method,
					"headers": {
						"x-rapidapi-key": geolocation.setting.rapidapi.key,
						"x-rapidapi-host": geolocation.setting.rapidapi.host,
					}
				}
			};
			if (type === 'reverse') {
				geolocation.fetchGeocoding.initService.url = `https://forward-reverse-geocoding.p.rapidapi.com/v1/reverse?lat=${geolocation.data.latitude}&lon=${geolocation.data.longitude}&accept-language=${geolocation.setting.rapidapi.countrycodes.toLowerCase()}&format=${geolocation.setting.rapidapi.format}`;
			} else if (type === 'search') {
				geolocation.fetchGeocoding.initService.url = `https://forward-reverse-geocoding.p.rapidapi.com/v1/search?q=${geolocation.input.address.value}&countrycodes=${geolocation.setting.rapidapi.countrycodes}&format=${geolocation.setting.rapidapi.format}&limit=7`;
			}
			break;
		case 'nominatim':
			geolocation.fetchGeocoding.initService = {
				nameService: 'nominatim',
				data: {},
			};
			if (type === 'reverse') {
				geolocation.fetchGeocoding.initService.url = `https://nominatim.openstreetmap.org/reverse.php?lat=${geolocation.data.latitude}&lon=${geolocation.data.longitude}&accept-language=${geolocation.setting.nominatim.countrycodes.toLowerCase()}&format=${geolocation.setting.nominatim.format}`;
			}
			break;
		case 'geoapifr':
			geolocation.fetchGeocoding.initService = {
				nameService: 'geoapifr',
				data: {},
			};
			if (type === 'reverse') {
				geolocation.fetchGeocoding.initService.url = `https://api-adresse.data.gouv.fr/reverse/?lat=${geolocation.data.latitude}&lon=${geolocation.data.longitude}`;
			} else if (type === 'search') {
				geolocation.fetchGeocoding.initService.url = `https://api-adresse.data.gouv.fr/search/?q=${geolocation.input.address.value}&limit=7&autocomplete=1`;
			}
			break;
		case 'bing':
			geolocation.fetchGeocoding.initService = {
				nameService: 'bing',
				data: {},
			};
			if (type === 'reverse') {
				geolocation.fetchGeocoding.initService.url = `http://dev.virtualearth.net/REST/v1/Locations/${geolocation.data.latitude},${geolocation.data.longitude}?o=${geolocation.setting.bing.format}`;
			} else if (type === 'search') {
				geolocation.fetchGeocoding.initService.url = `http://dev.virtualearth.net/REST/v1/Locations?key=${geolocation.setting.bing.key}&q=${geolocation.input.address.value}&o=${geolocation.setting.bing.format}&maxResults=7`;
			}
			break;
		case 'cache':
			console.log(`LocalStorage Service: enable`);
			break;
		case 'debug':
			geolocation.fetchGeocoding.initService = geolocation.debug.initService;
			geolocation.fetchGeocoding.initService.url = geolocation.debug.defaultServices[geolocation.debug.initService.type].url;
			geolocation.data = {
				latitude: geolocation.debug.data.latitude,
				longitude: geolocation.debug.data.longitude,
			};
			break;
	}
}

/**
 * Add and remove Attribute disabled
 * @param {HTMLElement} objectHtml
 */
function toggleDisabled(objectHtml) {
	objectHtml.forEach(object => {
		object.toggleAttribute('disabled');
	});
}

/**
 * fetch Reverse Geocoding API
 */
async function fetchGeocoding() {
	fetch(geolocation.fetchGeocoding.initService.url, geolocation.fetchGeocoding.initService.data)
		.then((response) => {
			geolocation.fetchGeocoding.response = response;
			if (response.ok) {
				console.log(`Response ok Service: ${geolocation.fetchGeocoding.initService.nameService} `);
				return response.json();
			} else {
				catchFetchGeocoding();
			}
		}).catch(err => {
			catchFetchGeocoding();
		})
		.then((data) => {
			geolocation.fetchGeocoding.data = data;
			if (data !== undefined) {
				formatPlace(data)
				setLocalStorage('fetchGeocoding', {
					address: geolocation.data.address,
					lat: geolocation.data.lat,
					lng: geolocation.data.lng,
				});
			}
			toggleDisabled([geolocation.input.geo, geolocation.input.address]);
		});
}

/**
 * Catching and handling exceptions
 */
function catchFetchGeocoding() {
	console.info(`Catch Service: ${geolocation.fetchGeocoding.initService.nameService} `);
	initService(geolocation.defaultServices[geolocation.fetchGeocoding.indexServices], geolocation.fetchGeocoding.type);
	if (geolocation.fetchGeocoding.indexServices < geolocation.fetchGeocoding.totalServices) {
		fetchGeocoding();
	} else {
		toggleDisabled([geolocation.input.geo, geolocation.input.address]);
		alert('Ce service est temporairement indisponible');
	}
}

/**
 * format Place in to geolocation.address
 *
 * @param {array} address
 */
function formatPlace(data) {
	switch (geolocation.fetchGeocoding.initService.nameService) {
		case 'bing':
			if (data.resourceSets.length > 0) {
				geolocation.data.address = data.resourceSets[0].resources[0].address.formattedAddress;
			}
			break;
		case 'geoapifr':
			if (data.features.length > 0) {
				geolocation.data.address = data.features[0].properties.label;
			}
			break;
		default:
			geolocation.data.address = data.address;
			break;
	}
	geolocation.input.address.value = geolocation.data.address;
}

/**
 * format Place in to geolocation.address
 *
 * @param {array} address
 */
function formatData(data) {
	switch (geolocation.fetchGeocoding.initService.nameService) {
		case 'bing':
			if (data.resourceSets.length > 0) {
				return data.resourceSets[0].resources;
			}
			break;
		case 'geoapifr':
			if (data.features.length > 0) {
				return data.features;
			}
			break;
		default:
			return geolocation.data.address = data;
			break;
	}
}

// The autoComplete.js Engine instance creator
const autoCompleteJS = new autoComplete({
	wrapper: false,
	debounce: 1000,
	data: {
		src: async (query) => {
			try {
				let source = await fetch(geolocation.fetchGeocoding.initService.url, geolocation.fetchGeocoding.initService.data);
				if (source.ok) {
					console.log(`Response ok Service: ${geolocation.fetchGeocoding.initService.nameService} `);
					let data = await source.json();
					return Object.keys(data).length === 0 ? [] : formatData(data);
				} else {
					initService(geolocation.defaultServices[geolocation.fetchGeocoding.indexServices], geolocation.fetchGeocoding.type);
					if (geolocation.fetchGeocoding.indexServices < geolocation.fetchGeocoding.totalServices) {
						let source = await fetch(geolocation.fetchGeocoding.initService.url, geolocation.fetchGeocoding.initService.data);
						if (source.ok) {
							console.log(`Response ok Service: ${geolocation.fetchGeocoding.initService.nameService} `);
							let data = await source.json();
							return Object.keys(data).length === 0 ? [] : formatData(data);
						}
					}
					alert('Ce service est temporairement indisponible');
				}
				return [];
			} catch (error) {
				initService(geolocation.defaultServices.search[geolocation.fetchGeocoding.indexServices], geolocation.fetchGeocoding.type);
				if (geolocation.fetchGeocoding.indexServices < geolocation.fetchGeocoding.totalServices) {
					let source = await fetch(geolocation.fetchGeocoding.initService.url, geolocation.fetchGeocoding.initService.data);
					if (source.ok) {
						console.log(`Response ok Service: ${geolocation.fetchGeocoding.initService.nameService} `);
						let data = await source.json();
						return Object.keys(data).length === 0 ? [] : formatData(data);
					}
				}
				alert('Ce service est temporairement indisponible');
				return [];
			}
		},
		keys: ["display_name", 'address', 'properties'],
		cache: false,
	},
	trigger: (query) => {
		if (query.length > geolocation.input.autoCompleteJS.maxlength) {
			localStorage.removeItem('fetchGeocoding');
			initService(geolocation.defaultServices.search[0], 'search');
			return true;
		}
		return false;
	},
	searchEngine: function (query, record) {
		return record;
	},
	resultsList: {
		element: (list, data) => {
			const info = document.createElement("p");
			if (data.results.length) {
				info.innerHTML = `Displaying <strong>${data.results.length}</strong> out of <strong>${data.matches.length}</strong> results`;
			} else {
				info.innerHTML = `Found <strong>${data.matches.length}</strong> matching results for <strong>"${data.query}"</strong>`;
			}
			list.prepend(info);
		},
		maxResults: 7, // max suported to api
		noResults: false,
	},
	resultItem: {
		tag: "li",
		class: "autoComplete_result",
		element: (item, data) => {
			switch (geolocation.fetchGeocoding.initService.nameService) {
				case 'bing':
					item.textContent = data.match.formattedAddress;
					break;
				case 'geoapifr':
					item.textContent = data.match.label;
					break;
			}
		},
	},
	events: {
		input: {
			selection(event) {
				let value = event.detail.selection.value;
				switch (geolocation.fetchGeocoding.initService.nameService) {
					case 'bing':
						autoCompleteJS.input.value = value.address.formattedAddress;
						geolocation.input.lat ? geolocation.input.lat.value = value.geocodePoints[0].coordinates[0] : '';
						geolocation.input.lng ? geolocation.input.lng.value = value.geocodePoints[0].coordinates[1] : '';
						break;
					case 'geoapifr':
						autoCompleteJS.input.value = value.properties.label;
						geolocation.input.lat ? geolocation.input.lat.value = value.geometry.coordinates[1] : '';
						geolocation.input.lng ? geolocation.input.lng.value = value.geometry.coordinates[0] : '';
						break;
					default:
						autoCompleteJS.input.value = value.display_name;
						geolocation.input.lat ? geolocation.input.lat.value = value.lat : '';
						geolocation.input.lng ? geolocation.input.lng.value = value.lon : '';
						break;
				}
				setLocalStorage('fetchGeocoding', {
					address: autoCompleteJS.input.value ,
					lat: geolocation.input.lat ? geolocation.input.lat.value : '',
					lng: geolocation.input.lng ? geolocation.input.lng.value : '',
				});
			},
		},
	},
});

/**
 * The setLocalStorage() method of the Storage interface, when passed a key name and value, will add that key to the given Storage object, or update that key's value if it already exists au format JSON.
 *
 * @param {string} name
 * @param {string} value
 */
function setLocalStorage(name, value) {
	localStorage.setItem(name, JSON.stringify(value))
}

/**
 * The getLocalStorage() method of the Storage interface, when passed a key name, will return that key's value, or null if the key does not exist, in the given Storage object.
 *
 * @param {string} name
 * @return The Object, Array, string, number, boolean, or null value corresponding
 */
function getLocalStorage(name) {
	let value = localStorage.getItem(name);
	if (value) {
		value = JSON.parse(value);
	}
	return value;
}

/**
 * The setDataLocalStorage() when passed a key name, will return that key's value, or null if the key does not exist, in the given geolocation.data object.
 *
 * @param {string} name
 * @return The Object, Array, string, number, boolean, or null value corresponding
 */
function setDataLocalStorage(name) {
	let dataGeocoding = getLocalStorage('fetchGeocoding');
	if (dataGeocoding) {
		geolocation.input.address.value = dataGeocoding.address;
		geolocation.input.lat.value = dataGeocoding.lat;
		geolocation.input.lng.value = dataGeocoding.lng
		geolocation.data = dataGeocoding;
		return geolocation.data;
	}
	return null;
}

/**
 * The setObjectInitService()
 *
 * @param {string} nameService
 * @param {string} type
 */
function setObjectfetchGeocoding(nameService, type = '') {
	if (geolocation.fetchGeocoding === undefined) { // defined fist init
		if (!setDataLocalStorage()) {
			geolocation.fetchGeocoding = {
				type: type,
				indexServices: 0,
				response: null,
				data: null,
			}
			geolocation.fetchGeocoding.totalServices = (type === 'reverse') ? geolocation.defaultServices.reverse.length : geolocation.defaultServices.search.length;
		} else {
			nameService = 'cache';
		}
	} else {
		geolocation.fetchGeocoding.indexServices = ++geolocation.fetchGeocoding.indexServices;
		(type === 'search' && geolocation.fetchGeocoding.indexServices === geolocation.defaultServices.search.length) ? geolocation.fetchGeocoding.indexServices = 0: '';
		nameService = geolocation.defaultServices[geolocation.fetchGeocoding.type][geolocation.fetchGeocoding.indexServices];
	}
	return nameService;
}

/**
 * Active debug mode
 */
if (geolocation.debug.enable) {
	console.warn('Mode debug enable !!!!');
	if (!geolocation.debug.cache) {
		localStorage.removeItem('fetchGeocoding');
	}
	if (geolocation.debug.initService.type === 'reverse') {
		initService('debug', geolocation.debug.initService.type);
		if (geolocation.fetchGeocoding !== undefined) {
			toggleDisabled([geolocation.input.geo, geolocation.input.address]);
			fetchGeocoding();
		}
	}
	if (geolocation.debug.log) {
		console.debug(geolocation);
	}
}
