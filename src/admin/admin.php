<?php
/**
 * ACF include files and init class.
 *
 * @package WP_GEO_Query
 * @since 1.0.0
 */

// Add Page Custome settings.
require WPGQ_PATH . '/admin/acf/options-page/options-page.php';

// Add defaut settings for map field.
require WPGQ_PATH . '/admin/acf/class/class-acf-map-field.php';
Acf_Map_Field::init();
