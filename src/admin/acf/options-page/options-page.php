<?php
/**
 * ACF functions.
 *
 * @package WP_GEO_Query
 * @subpackage ACF
 * @since 1.0.0
 */

/**
 * Fires after ACF is fully initialized.
 *
 * This action is similar to the WordPress init action,
 * and should be used to extend or register items such as Blocks,
 * Forms and Options Pages.
 *
 * @return void
 */
function wpgq_acf_init_admin() {

	if ( function_exists( 'acf_add_options_page' ) ) {

		acf_add_options_sub_page(
			array(
				'page_title'  => __( 'Settings', 'wp-geo-query' ),
				'menu_title'  => __( 'WP Geo Query', 'wp-geo-query' ),
				'parent_slug' => 'options-general.php',
				'menu_slug'   => 'wp-geo-query',
			)
		);
	}
}
add_action( 'admin_menu', 'wpgq_acf_init_admin' );

/**
 * Save and update the ACF fields in a json file
 *
 * @link https://www.advancedcustomfields.com/resources/local-json/
 *
 * @param string $path Directory path.
 * @return string $path
 */
function wpgq_acf_json_save_point( $path ) {

	global $post;

	if ( isset( $post->post_name ) && 'group_61166d156a6ba' === $post->post_name ) {

		$path = WPGQ_PATH . '/admin/acf-json';
	}

	return $path;
}
add_filter( 'acf/settings/save_json', 'wpgq_acf_json_save_point', 99999999, 1 );

/**
 * Loading the ACF fields from the backup directory
 *
 * @link https://www.advancedcustomfields.com/resources/local-json/
 *
 * @param array $paths list of directory paths.
 * @return array $paths
 */
function wpgq_acf_json_load_point( $paths ) {

	global $post;

	if ( isset( $post->post_name ) && 'group_61166d156a6ba' === $post->post_name ) {

		unset( $paths[0] );

		$paths[] = WPGQ_PATH . '/admin/acf-json';
	}

	return $paths;
}
add_filter( 'acf/settings/load_json', 'wpgq_acf_json_load_point', 10, 1 );
