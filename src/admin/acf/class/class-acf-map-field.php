<?php
/**
 * Acf_Map_Field class
 *
 * @package WP_Geo_Query
 * @subpackage Acf
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Acf_Map_Field.
 *
 * The main Acf_Map_Field handler class is responsible for initializing.
 * The class registers and all the components required to run the Acf_Map_Field.
 *
 * @version 1.0.0
 */
class Acf_Map_Field {

	/**
	 * Instance.
	 *
	 * Holds the plugin instance.
	 *
	 * @since  1.0.0
	 * @access public
	 * @static
	 *
	 * @var Acf_Map_Field|null
	 */
	public static $instance = null;

	/**
	 * Init.
	 *
	 * Ensures only one instance of the Acf_Map_Field class is loaded or can be loaded.
	 *
	 * @since  1.0.0
	 * @access public
	 * @static
	 *
	 * @return Acf_Map_Field An instance of the class.
	 */
	public static function init() {

		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Constructor.
	 *
	 * Initializing Acf_Map_Field.
	 *
	 * @since  1.0.0
	 * @access private
	 */
	private function __construct() {

		add_filter( 'acf/fields/google_map/api', array( $this, 'acf_google_map_api' ) );
		add_action( 'acf/render_field_settings/type=google_map', array( $this, 'acf_google_map_key_render_field_settings' ) );
		add_filter( 'acf/validate_field/type=google_map', array( $this, 'acf_google_map_key_validate_value' ) );
		add_filter( 'acf/update_value/type=open_street_map', array( $this, 'acf_field_map_update_value' ), 10, 4 );
		add_filter( 'acf/update_value/type=google_map', array( $this, 'acf_field_map_update_value' ), 10, 4 );
	}

	/**
	 * Used to modify the url parameters used to load the Google Maps JS API.
	 *
	 * Primarily, this filter exists to set a Google API key, however,
	 * it may also be used to customize other_params used in the google.load function.
	 *
	 * @link https://www.advancedcustomfields.com/resources/acf-fields-google_map-api/
	 *
	 * @since  1.0.0
	 * @access protected
	 *
	 * @param array $args Array of url parameters.
	 *
	 * @return array
	 */
	public function acf_google_map_api( $args ) {
		$args['key'] = get_option( 'acf_google_map_key', true );
		return $args;
	}

	/**
	 * Adding custom settings to custom fields (ACF).
	 *
	 * @link https://www.advancedcustomfields.com/resources/adding-custom-settings-fields/
	 *
	 * @since  1.0.0
	 * @access protected
	 *
	 * @param array $field list the default parameters.
	 * @return void
	 */
	public function acf_google_map_key_render_field_settings( $field ) {

		$default_value = get_option( 'acf_google_map_key', true );

		if ( ! is_string( $default_value ) ) {
			$default_value = '';
		}

		acf_render_field_setting(
			$field,
			array(
				'label'         => __( 'Google KEY API', 'wp-geo-query' ),
				'instructions'  => '',
				'type'          => 'text',
				'name'          => 'google_map_key',
				'ui'            => 1,
				'class'         => 'field-google-map-key',
				'default_value' => $default_value,
			),
			true
		);
	}

	/**
	 * Used to perform custom validation on the field’s $value before it is saved into the database.
	 *
	 * This function will append default settings to a field
	 *
	 * @type  filter ("acf/validate_field/type={$this->name}")
	 *
	 * @link https://www.advancedcustomfields.com/resources/acf-validate_value/
	 *
	 * @since 1.0.0
	 * @access protected
	 *
	 * @param array $field The field value.
	 * @return array $field
	 */
	public function acf_google_map_key_validate_value( $field ) {

		if ( ! empty( $field['google_map_key'] ) ) {

			update_option( 'acf_google_map_key', $field['google_map_key'] );
		}
		return $field;
	}

	/**
	 * Filters the $value before it is updated.
	 *
	 * @link https://www.advancedcustomfields.com/resources/acf-update_value/
	 *
	 * @since 1.0.0
	 * @access protected
	 *
	 * @param mixed  $value The value to update.
	 * @param string $post_id The post ID for this value.
	 * @param array  $field The field array.
	 */
	public function acf_field_map_update_value( $value, $post_id, $field ) {

		if ( ! empty( $value ) && is_array( $value ) && isset( $field['_name'] ) ) {

			if ( isset( $value['lng'] ) ) {
				update_post_meta( $post_id, $field['_name'] . '_lng', $value['lng'] );
			}

			if ( isset( $value['lat'] ) ) {
				update_post_meta( $post_id, $field['_name'] . '_lat', $value['lat'] );
			}

			if ( isset( $value['label'] ) ) {
				update_post_meta( $post_id, $field['_name'] . '_address', $value['label'] );
			}
		}

		return $value;
	}
}
