<?php
/**
 * Functions.
 *
 * @package WP_GEO_Query
 * @since 1.0.0
 */

/**
 * Filters the query variables allowed before processing.
 *
 * Allows (publicly allowed) query vars to be added, removed, or changed prior
 * to executing the query. Needed to allow custom rewrite rules using your own arguments
 * to work, or any other custom query variables you want to be publicly available.
 *
 * @package WP_Geo_Query
 * @since 1.0.0
 *
 * @param string[] $public_query_vars The array of allowed query variable names.
 */
function wpgq_query_vars( $public_query_vars ) {

	$public_query_vars[] = 'geo-query';

	return $public_query_vars;
}
add_filter( 'query_vars', 'wpgq_query_vars', 10, 1 );

/**
 * Caluclate distance by two points.
 *
 * @param int         $lat1 latitude coordinates.
 * @param int         $lon1 longitude coordinates.
 * @param int         $lat2 latitude coordinates.
 * @param int         $lon2 longitude coordinates.
 * @param string|bool $unit Unit of length.
 * @param array       $number_format List Arguments.
 *                              - decimals.
 *                              - decimal_separator.
 *                              - thousands_separator.
 * @return string
 */
function wpgq_get_distance_by_points( $lat1, $lon1, $lat2, $lon2, $unit = false, $number_format = array() ) {

	$distance = 0;

	if ( $lat1 !== $lat2 && $lon1 !== $lon2 ) {

		$number_format = array_merge(
			array(
				'decimals'            => '2',
				'decimal_separator'   => '.',
				'thousands_separator' => ' ',
			),
			$number_format
		);

		$theta = $lon1 - $lon2;
		$dist  = sin( deg2rad( $lat1 ) ) * sin( deg2rad( $lat2 ) ) + cos( deg2rad( $lat1 ) ) * cos( deg2rad( $lat2 ) ) * cos( deg2rad( $theta ) );
		$dist  = acos( $dist );
		$dist  = rad2deg( $dist );
		$miles = $dist * 60 * 1.1515;

		switch ( strtoupper( $unit ) ) {
			case 'M':
				$distance = $miles;
				break;
			case 'N':
				$distance = ( $miles * 0.8684 );
				break;
			default:
				$distance = ( $miles * 1.609344 );
				break;
		}

		$distance = number_format( $distance, $number_format['decimals'], $number_format['decimal_separator'], $number_format['thousands_separator'] ) . ( ( ! $unit ) ? '' : $unit );
	}

	return $distance;
}
