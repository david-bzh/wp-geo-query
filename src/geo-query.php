<?php
/**
 * Include files.
 *
 * @package WP_GEO_Query
 * @since 1.0.0
 */

/**
 * Fires after WordPress has finished loading but before any headers are sent.
 *
 * Most of WP is loaded at this stage, and the user is authenticated. WP continues
 * to load on the {@see 'init'} hook that follows (e.g. widgets), and many plugins instantiate
 * themselves on it for all sorts of reasons (e.g. they need a user, a taxonomy, etc.).
 *
 * If you wish to plug an action once WP is loaded, use the {@see 'wp_loaded'} hook below.
 *
 * @since 1.0.0
 */
function wpgq_init() {

	include WPGQ_PATH . '/inc/functions.php';

	if ( is_admin() && is_user_logged_in() ) {

		include WPGQ_PATH . '/admin/admin.php';

	} if ( ! is_admin() ) {

		include WPGQ_PATH . 'template/template.php';
	}
}
add_action( 'init', 'wpgq_init' );
