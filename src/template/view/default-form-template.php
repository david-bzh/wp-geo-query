<?php
/**
 * The default-form-template.php template.
 *
 * Used any time that get_search_form() is called.
 *
 * @link https://developer.wordpress.org/reference/functions/wp_unique_id/
 * @link https://developer.wordpress.org/reference/functions/get_search_form/
 *
 * @package WP_Query_Geo
 * @since 1.0.0
 */

/*
 * Generate a unique ID for each form and a string containing an aria-label
 * if one was passed to get_search_form() in the args array.
 */

$wp_geo_args       = $args['geo_query'];
$wp_geo_unique_id  = wp_unique_id( 'wp-geo-form-' );
$wp_geo_aria_label = ! empty( $args['aria_label'] ) ? 'aria-label="' . esc_attr( $args['aria_label'] ) . '"' : '';
$values            = get_query_var( 'geo-query' );

?>
<div class="container-fuild default-form-template" id="wp-geo-form" style="visibility: hidden;">

	<form role="search" method="get" action="<?php echo ( isset( $wp_geo_args['action'] ) ) ? esc_url( $wp_geo_args['action'] ) : ''; ?>" <?php echo $wp_geo_aria_label; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- Escaped above. ?>  class="d-flex w-100">

		<div class="col-auto pl-0">
			<div class="input-group mb-3 border rounded">
			<div class="autoComplete_wrapper input-group">
				<input class="form-control border-0 fs-9 btn-address m-0" name="" type="text" placeholder="<?php echo esc_attr_x( 'Your Address ...', 'placeholder address', 'wp-geo-query' ); ?>" aria-label="<?php echo esc_attr_x( 'Your Address ...', 'label address', 'wp-geo-query' ); ?>" aria-describedby="<?php echo esc_attr_x( 'Your Address ...', 'label address', 'wp-geo-query' ); ?>" spellcheck=false  autocorrect="off"  autocomplete="off" autocapitalize="off"  maxlength="2048"  tabindex="1" id="autoComplete" required  oninvalid="this.setCustomValidity('<?php echo esc_attr_x( 'Please fill in this field.', 'text validity', 'wp-geo-query' ); ?>')" oninput="this.setCustomValidity('')" />
				<button class="btn btn-geolocation rounded-0" type="button" id="btnGeo">
				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16"><path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/><path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/></svg>
				</button>
			</div>
			</div>
			<div class="input-group-hidden" style="display: none;">
				<input class="btn-geolocation-lat" type="hidden" values="<?php echo ( isset( $values['lat'] ) ) ? esc_attr( $values['lat'] ) : ''; ?>" name="geo-query[lat]">
				<input class="btn-geolocation-lng"  type="hidden" values="<?php echo ( isset( $values['lng'] ) ) ? esc_attr( $values['lng'] ) : ''; ?>" name="geo-query[lng]">
			</div>
		</div>

		<?php if ( isset( $wp_geo_args['radius'] ) && $wp_geo_args['radius'] ) { ?>

			<div class="col-auto">
				<label for="customRange1" class="form-label my-0 fs-9 px-4 mx-1 color-white" id="label-range"><?php echo esc_attr_x( 'Distance Range', 'label range', 'wp-geo-query' ); ?></label>
			</div>

			<div class="col-auto p-0">
				<?php $number_range_min = esc_attr( get_option( 'options_value_range_min', true ) ); ?>
				<?php $number_range_max = esc_attr( get_option( 'options_value_range_max', true ) ); ?>
				<div id="nouislider"></div>
				<input style="display: none;" class="form-range px-2 w-100" name="geo-query[radius]" type="range" min="<?php echo $number_range_min; ?>" max="<?php echo $number_range_max ?>"  value="<?php echo ( isset( $values['radius'] ) ) ? esc_attr( $values['radius'] ) : $number_range_min;  // phpcs:ignore ?>" id="customRange1"/>
				<div class="col-auto w-100 d-flex p-0 my-3"">
					<span class="input-group d-block fs-9 color-white" id="span-range-min w-50"><?php echo  $number_range_min; // phpcs:ignore ?><?php echo _e( ' Km', 'wp-geo-query' ); ?></span>
					<span class="input-group d-block text-right fs-9 color-white" id="span-range-max"><?php echo $number_range_max // phpcs:ignore ?><?php echo _e( ' Km', 'wp-geo-query' ); ?></span>
				</div>
			</div>

		<?php } ?>

		<div class="col-auto text-right pr-0">
			<button class="btn btn-primary fs-9" type="submit"><?php echo esc_attr_x( 'Search', 'submit button', 'wp-geo-query' ); ?></button>
		</div>

		<div class="input-group-hidden" style="display: none;">
			<input class="btn-geolocation-lat" type="hidden" value="<?php echo ( isset( $values['order'] ) ) ? esc_attr( $values['order'] ) : 'DESC'; ?>" name="geo-query[order]">
		</div>

	</form>
</div>
