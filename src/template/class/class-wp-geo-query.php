<?php
/**
 * WP_Geo_Query class
 *
 * @package WP_Geo_Query
 * @since   1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * WP_Geo_Query.
 *
 * The main WP_Geo_Query handler class is responsible for initializing.
 * The class registers and all the components required to run the WP_Geo_Query.
 *
 * @version 1.0.0
 */
class WP_Geo_Query {


	/**
	 * Holds the plugin instance.
	 *
	 * @since  1.0.0
	 * @access public
	 * @static
	 *
	 * @var WP_Geo_Query|null
	 */
	public static $instance = null;

	/**
	 * Array of key
	 *
	 * @since  1.0.0
	 * @access public
	 *
	 * @var array
	 */
	private $query_geo = array(
		'lat',
		'lng',
		'radius',
		'order',
	);

	/**
	 * The WP_Query Instance (passed by reference).
	 *
	 * @since  1.0.0
	 * @access private
	 *
	 * @var WP_Query|null
	 */
	private $query = null;

	/**
	 * Associative array of the clauses for the query.
	 *
	 * @since  1.0.0
	 * @access public
	 *
	 * @var string|array
	 */
	public $clauses = array();

	/**
	 * Init.
	 *
	 * Ensures only one instance of the WP_Geo_Query class is loaded or can be loaded.
	 *
	 * @since  1.0.0
	 * @access public
	 * @static
	 *
	 * @return WP_Geo_Query An instance of the class.
	 */
	public static function init() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Constructor.
	 *
	 * Initializing WP_Geo_Query.
	 *
	 * @since  1.0.0
	 * @access private
	 */
	private function __construct() {
	}

	/**
	 * Filters all query clauses at once, for convenience.
	 *
	 * @since  1.0.0
	 *
	 * @param string[] $clauses Associative array of the clauses for the query.
	 * @param WP_Query $query   The WP_Query instance (passed by reference).
	 * @return array
	 */
	public function posts_clauses( $clauses, $query ) {
		$this->clauses = $clauses;
		$this->query   = $query;

		if ( $this->wp_geo_query_validate() ) {
			return $this->clauses;
		}

		return $this->clauses;
	}

	/**
	 * Validate if WP_Query contains geo_query
	 *
	 * @return bool
	 */
	private function wp_geo_query_validate() {
		$fields = $this->query->get( 'geo-query' );

		if ( empty( $fields ) ) {
			return false;
		}

		if ( ! isset( $fields['radius'] ) ) {
			$fields['radius'] = get_option( 'options_value_range_min', true );
		}

		foreach ( $this->query_geo as $key ) {
			if ( ! isset( $fields[ $key ] ) || empty( $fields[ $key ] ) ) {
				return false;
			}

			$this->query_geo[ $key ] = $fields[ $key ];
		}

		$this->set_where();
		$this->set_join();
		$this->set_fields();

		if ( isset( $fields['order'] ) ) {
			$this->query_geo['order'] = $fields['order'];
			$this->set_orderby();
		}

		return true;
	}

	/**
	 * Filters the WHERE clause of the query.
	 *
	 * @return void
	 */
	private function set_where() {
		$this->clauses['where'] .= ' AND' . $this->get_distance_query() . ' <= ' . floatval( $this->query_geo['radius'] );
	}

	/**
	 * Filters the JOIN clause of the query.
	 *
	 * @return void
	 */
	private function set_join() {
		$this->clauses['join'] .= "
			LEFT JOIN wp_postmeta lats
				ON wp_posts.id = lats.post_id
				AND lats.meta_key = 'geo_query_lat'
			LEFT JOIN wp_postmeta lngs
				ON wp_posts.id = lngs.post_id
				AND lngs.meta_key = 'geo_query_lng'
		";
	}

	/**
	 * Filters the ORDER BY clause of the query.
	 *
	 * @return void
	 */
	private function set_orderby() {

		$this->clauses['orderby'] = str_replace(
			array(
				'wp_posts.post_date DESC',
				'wp_posts.post_date ASC',
			),
			'',
			$this->clauses['orderby']
		);

		$this->clauses['orderby'] .= ( empty( $this->clauses['orderby'] ) ? '' : ', ' ) . '-distance ' . ( ( 'DESC' === $this->query_geo['order'] ) ? $this->query_geo['order'] : 'ASC' );
	}

	/**
	 * Filters the SELECT clause of the query.
	 *
	 * @return void
	 */
	private function set_fields() {
		$this->clauses['fields'] = '
			wp_posts.*,
			lats.meta_value AS geo_query_lat,
			lngs.meta_value AS geo_query_lng,
			' . $this->get_distance_query() . '
				AS distance';
	}

	/**
	 * Return the query SQL for caluclate distance.
	 *
	 * @return string
	 */
	private function get_distance_query() {
		return '( 6371 * acos(
		cos( radians( ' . floatval( $this->query_geo['lat'] ) . ' ) )
		 * cos( radians( lats.meta_value ) )
		 * cos( radians( lngs.meta_value ) - radians( ' . floatval( $this->query_geo['lng'] ) . ' ) )
		 + sin( radians( ' . floatval( $this->query_geo['lat'] ) . ' ) )
		 * sin( radians( lats.meta_value ) )
		) )';
	}
}
