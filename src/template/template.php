<?php
/**
 * ACF include files and init class.
 *
 * @package WP_Query_Geo
 * @since 1.0.0
 */

// Include WP_Query_Geo class.
if ( ! class_exists( 'WP_Query_Geo' ) && ( ( isset( $_GET['geo-query'] ) ) || ( isset( $_POST['geo-query'] ) ) ) ) { // phpcs:ignore

	include WPGQ_PATH . 'template/class/class-wp-geo-query.php';
	add_filter( 'posts_clauses', array( WP_Geo_Query::init(), 'posts_clauses' ), 10, 2 );
}

/**
 * Filters the HTML output of the search form.
 *
 * @since 1.0.0
 *
 * @param string $form The search form HTML output.
 * @param array  $args The array of arguments for building the search form.
 *                     See get_search_form() for information on accepted arguments.
 */
function wpgq_get_search_form( $form, $args ) {

	global $wpgq_load_enable;

	if ( isset( $args['geo_query'] ) ) {

		if ( ! $wpgq_load_enable ) {
			return '';
		}

		$args_geo_query = $args['geo_query'];

		$form_template = '';

		if ( isset( $args_geo_query['template'] ) ) {
			$form_template = locate_template( str_replace( get_stylesheet_directory(), '', $args_geo_query['template'] ) );
		}

		if ( '' === $form_template ) {

			$form_template = WPGQ_PATH . 'template/view/default-form-template.php';
		}

		ob_start();

		require $form_template;

		$form = ob_get_clean();
	}

	return $form;
}
add_filter( 'get_search_form', 'wpgq_get_search_form', 11, 2 );

/**
 * Fires when scripts and styles are enqueued.
 *
 * @since 1.0.0
 */
function wpgq_wp_enqueue_scripts() {

	global $wpgq_load_enable;

	$wpgq_load_enable = false;

	$geo_query_var = get_query_var( 'geo-query' );

	if ( ! empty( $geo_query_var ) ) {
		$wpgq_load_enable = true;

	} elseif ( is_archive() || is_single() ) {

		$post = get_post();

		if ( ! empty( $post ) ) {

			$form_enable_post_type = get_field( 'form_enable_post_type', 'options' );
			foreach ( $form_enable_post_type as $post_type ) {
				if ( $post->post_type === $post_type ) {
					$wpgq_load_enable = true;
					break;
				}
			}
		}
	} elseif ( is_page() ) {

		$form_enable_page = get_field( 'form_enable_page', 'options' );
		foreach ( $form_enable_page as $page ) {
			if ( is_page( $page ) ) {
				$wpgq_load_enable = true;
				break;
			}
		}
	}

	if ( $wpgq_load_enable ) {

		wp_register_style( 'wp-geo-form-nouislider', WPGQ_URI . 'assets/css/nouislider.min.css', array(), '15.4.0', 'all' );
		wp_register_style( 'wp-geo-form-autocomplete', WPGQ_URI . 'assets/css/autoComplete.min.css', array(), '1.0.0', 'all' );

		wp_register_style(
			'wp-geo-form-style',
			WPGQ_URI . 'assets/css/geo-query.css',
			array(
				'wp-geo-form-nouislider',
				'wp-geo-form-autocomplete',
			),
			'10.2.6',
			'all'
		);
		wp_enqueue_style( 'wp-geo-form-style' );

		wp_register_script( 'wp-geo-form-nouislider', WPGQ_URI . 'assets/js/nouislider.min.js', array(), '15.4.0', true );
		wp_register_script( 'wp-geo-form-autocomplete', WPGQ_URI . 'assets/js/autoComplete.min.js', array(), '10.2.6', true );

		wp_register_script(
			'wp-geo-form-script',
			WPGQ_URI . 'assets/js/geolocation.js',
			array(
				'wp-geo-form-nouislider',
				'wp-geo-form-autocomplete',
			),
			'1.0.0',
			true
		);
		wp_enqueue_script( 'wp-geo-form-script' );
	}
}
add_action( 'wp_enqueue_scripts', 'wpgq_wp_enqueue_scripts' );

/**
 * Filters the HTML script tag of an enqueued script.
 *
 * @since 1.0.0
 *
 * @param string $tag    The `<script>` tag for the enqueued script.
 * @param string $handle The script's registered handle.
 */
function wpgq_script_loader_tag( $tag, $handle ) {

	if ( 'wp-geo-form-script' === $handle ) {
		$tag = str_replace( '<script', '<script defer type="module"', $tag );
	}

	return $tag;
}
add_filter( 'script_loader_tag', 'wpgq_script_loader_tag', 10, 2 );

/**
 * Fire the wp_head action.
 *
 * @since 1.0.0
 */
function wpgq_wp_head() {

	if ( function_exists( 'get_field' ) ) {

		echo '<!-- ACF GEO --var-color  --> <script> var SETTING_SERVICES = { ';

		if ( have_rows( 'service', 'options' ) ) :

			while ( have_rows( 'service', 'options' ) ) :
				the_row();

				echo esc_attr( get_sub_field( 'name' ) ) . ':{';

				if ( have_rows( 'parameters' ) ) :

					while ( have_rows( 'parameters' ) ) :
						the_row();

						echo esc_attr( get_sub_field( 'name' ) ) . ':\'' . esc_attr( get_sub_field( 'value' ) ) . '\',';

					endwhile;

					endif;

					echo 'search :\'' . ( ( get_sub_field( 'name' ) !== 'nominatim' ) ? 'true' : 'false' ) . '\',';
					echo 'reverse :\'true\',';
					echo 'enable :\'' . ( ( get_sub_field( 'enable' ) ) ? 'true' : 'false' ) . '\',';
					echo 'countrycodes :\'FR\',';
					echo 'format :\'json\',';

				echo '},';

			endwhile;

		endif;

		echo '};';

		$number_range_min = get_option( 'options_value_range_min', true );
		$values           = get_query_var( 'geo-query' );

		echo 'var SETTING_NOUISLIDER = {
			start: ' . ( ( isset( $values['radius'] ) ) ? esc_attr( $values['radius'] ) : esc_attr( $number_range_min ) ) . ',
			step: 1,
			connect: true,
			tooltips:[{to : function ( input ) { return `${input.toFixed(0)} km`;}}],
			range: {
				\'min\': ' . esc_attr( $number_range_min ) . ',
				\'max\': ' . esc_attr( get_option( 'options_value_range_max', true ) ) . '
			},
		}';

		echo '</script><!-- End WPGQEO -->';
	}
}
add_action( 'wp_head', 'wpgq_wp_head' );
