<?php
/**
 * Plugin Name:     WP Geo Query
 * Description:     Modify the WP_Query to filter the geo_query parameter.
 * Author:          David
 * Text Domain:     wp-geo-query
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package WP_Geo_Query
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'WPGQ_PATH', plugin_dir_path( __FILE__ ) . 'src/' );

define( 'WPGQ_URI', plugin_dir_url( __FILE__ ) . 'src/' );

define( 'WPGQ_VERSION', '1.0.0' );

/**
 * Fires once activated plugins have loaded.
 *
 * @source https://developer.wordpress.org/reference/hooks/plugins_loaded/
 *
 * Pluggable functions are also available at this point in the loading order.
 */
function wpgq_setup() {

	load_plugin_textdomain( 'wp-geo-query' );

	require WPGQ_PATH . '/geo-query.php';
}
add_action( 'plugins_loaded', 'wpgq_setup', 11 );
